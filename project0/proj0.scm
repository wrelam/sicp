;;; Student: Walt Elam
;;; TA:
;;; Section:
;;; Project: 0

;;; Part: 2
;;; The following test cases explore the evaluation of simple expressions
;;;

-37
;Value: -37

(* 3 4)
;Value: 12

(> 10 9.7)
;Value: #t

(- (if (> 3 4)
       7
       10)
   (/ 16 10))
;Value: 42/5

(* (- 25 20)
   (+ 6 3))
;Value: 45

+
;Value 14: #[arity-dispatched-procedure 14]

(define double (lambda (x) (* 2 x)))
;Value: double

double
;Value 15: #[compound-procedure 15 double]

(define c 4)
;Value: c

c
;Value: 4

(double c)
;Value: 8

c
;Value: 4

(double (double (+ c 5)))
;Value: 36

(define times-2 double)
;Value: times-2

(times-2 c)
;Value: 8

(define d c)
;Value: d

(= c d)
;Value: #t

(cond ((>= c 2) d)
      ((= c (- d 5) (+ c d)))
      (else (abs (- c d))))
;Value: 4

;;; Part: 3
;;; Demonstrates pretty printing vs. not. Readable code is good code.
;;;

;;; Pretty
(define abs
  (lambda (a)
    (if (> a 0)
	a
	(- a))))

;;; Not pretty
(define abs (lambda (a) (if (> a 0) a (- a))))

;;; Part: 5
;;; Documentation and Administrative Questions
;;;

;1. According to the Don't Panic manual, how do you invoke the stepper?  What is
;the difference between the stepper and the debugger?
;
;    The stepper is invoked with M-s in the same manner that you would evaluate
;    a line of Scheme with C-x C-e. The debugger is used to backtrack and figure
;    out what happened while the stepper is better used for finding out what was
;    going on before the error happened.
;
;2. According to the Guide to MIT Scheme, which of the words in the scheme
;expressions you evaluated in Part 2 above are "special forms"?
;
;   According to the Guide to MIT Scheme, the special forms from part 2 are:
;   define, cond, and else
;
;3. Referring to the course policy on collaboration, describe the policy on the
;use of "bibles."
;
;   The use of bibles is legitimate as a source of supplementary
;   problems for additional practice, to try to test and increase your
;   understanding of the material. However, is it not legitimate to use them as
;   a source of code for solutions to any material in this course.
;
;4. List three people with whom you might collaborate on projects this term.
;You are not required to actively collaborate with these people, we just want
;you to start thinking about what collaboration means in this course.
;
;   I might collaborate with John, John, or John.
;
;5. Locate the list of announcements for the class. What do these announcements
;say about recitation attendance during the first week of classes?
;
;   You must attend recitations during the first week of class so that you can
;   be assigned to a group/session for tutorials.
;
;6. What are the three methods for controlling complexity described in the
;learning objectives section of the course objectives and outcomes? List one
;example from each category.
;
;   The major mechanisms for controlling complexity are building abstractions,
;   controlling interactions through conventional interfaces, and designing new
;   languages. One method of building abstractions is through the special form
;   define in Scheme which allows you to name an expressions for later use.
;   Conventional interfaces are demonstrated with the manner in which you define
;   and evaluate a new procedure by passing arguments to it and having a result
;   returned to you. Designing a new language can help you create features that
;   don't exist in other languages; three key parts are identifying a
;   vocabulary, a syntax, and semantics for your language.
;
;7. What does the MIT Scheme Reference Manual say about treatment of upper and
;lower case expressions?
;
;   The MIT Scheme Reference Manual states that Scheme doesn't distinguish
;   between uppercase and lowercase forms of a letter except within character
;   and string constants.
;
;8. What are the Edwin commands for creating a new file, and for saving a file?
;What is the difference between the *scheme* buffer and the *transcript* buffer?
;
;   The Edwin command for creating a new file is C-x b <filename>, saving a file
;   is C-x C-s. The *scheme* buffer is the best place for testing Scheme
;   expressions as it will evaluate the expression in the same buffer; it also
;   displays the value of expressions which Edwin evaluates from a separate
;   buffer. The *transcript* buffer is a read-only buffer which contains a
;   history of the expressions which you have evaluated.
;
