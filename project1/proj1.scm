;;; Student: Walt Elam
;;; TA:
;;; Section:
;;; Project: 1

;;; Baseball robot simulation
(define square (lambda (x) (* x x)))

;; these are constants that will be useful to us
(define gravity 9.8)
(define pi 3.14159)

;;; Part: 1
;;; Write a procedure that takes as input values for a, v, u, and t and returns 
;;; as output the position of the baseball at time t.

(define position (lambda (a v u t)
    (+ (* (/ 1 2.0) a (square t)) 
       (* v t)
       u)))

;; Test cases
; (position 0 0 0 0) --> 0
; Value: 0

; (position 0 0 20 0) --> 20
; Value: 20

; (position 0 5 10 10) --> 60
; Value: 60

; (position 2 2 2 2) --> ??
; Value 10

; (position 5 5 5 5) --> ??
; Value: 185/2

;;; Part 2:
;;; Write one procedure to find a root of the quadratic equation, and another 
;;; procedure to find the other root

(define root1 (lambda (a b c)
  (if (= a 0)
      #f
      (/ (+ (- b) (sqrt (- (square b) (* 4 a c))))
         (* 2 a)))))

(define root2 (lambda (a b c)
  (if (= a 0)
      #f
      (/ (- (- b) (sqrt (- (square b) (* 4 a c))))
         (* 2 a)))))

;; Test cases

; (root1 5 3 6)
(root1 5 3 6)
;Value: -3/10+1.0535653752852738i

; (root2 5 3 6)
(root2 5 3 6)
;Value: -3/10-1.0535653752852738i

;; Problem 3
;;; Part: 3
;;; Write a procedure that computes how long the baseball will be in flight.
;;; Write a procedure that computes the time for the balal to reach a given
;;; target elevation.

; Time at impact is the t when position is 0, given an acceleration of 9.8m/s^2
; an initial velocity of vertical-velocity, and an initial position of elevation
(define time-to-impact
  (lambda (vertical-velocity elevation)
    (define r1 (root1 (/ (- gravity) 2) vertical-velocity elevation))
    (define r2 (root2 (/ (- gravity) 2) vertical-velocity elevation))
    (if (> r1 0) r1 r2)))
    
;; Note that if we want to know when the ball drops to a particular height r 
;; (for receiver), we have

(define time-to-height
  (lambda (vertical-velocity elevation target-elevation)
    (time-to-impact vertical-velocity (- elevation target-elevation))))

;; Test cases
; (time-to-impact 20 60) --> 6.092
(time-to-impact 20 60)

;(time-to-height 20 60 0) --> 6.092
(time-to-height 20 60 0)

;; Part 4: Write a procedure that returns the lateral distance the baseball
;; thrown with given velocity, angle, and initial elevation will travel before
;; hitting the ground

;; once we can solve for t_impact, we can use it to figure out how far the ball went

;; conversion procedure
(define degree2radian
  (lambda (deg)
    (/ (*  deg pi) 180.)))

(define radian2degree
  (lambda (rad)
    (* rad (/ 180. pi))))

(define travel-distance-simple
  (lambda (elevation velocity angle)
  (define rad (degree2radian angle))
  (position 0 
	    (* velocity (cos rad))
	    0 
	    (time-to-height (* velocity (sin rad)) elevation 0))))

;; Test cases
; (travel-distance-simple 0 50 25) --> 195.419
(travel-distance-simple 0 50 25)


;; let's try this out for some example values.  Note that we are going to 
;; do everything in metric units, but for quaint reasons it is easier to think
;; about things in English units, so we will need some conversions.

(define meters-to-feet
  (lambda (m)
    (/ (* m 39.6) 12)))

(define feet-to-meters
  (lambda (f)
    (/ (* f 12) 39.6)))

(define hours-to-seconds
  (lambda (h)
    (* h 3600)))

(define seconds-to-hours
  (lambda (s)
    (/ s 3600)))

;; what is time to impact for a ball hit at a height of 1 meter
;; with a velocity of 45 m/s (which is about 100 miles/hour)

;; at an angle of 0 (straight horizontal)
; .4518s
(time-to-impact (* 45 (sin (degree2radian 0))) 1)

;; at an angle of (/ pi 2) radians or 90 degrees (straight vertical)
; 9.206s
(time-to-impact (* 45 (sin (degree2radian 90))) 1)

;; at an angle of (/ pi 4) radians or 45 degrees
; 6.525s
(time-to-impact (* 45 (sin (degree2radian 45))) 1)

;; what is the distance traveled in each case?
;; record both in meters and in feet
; 20.328 m, 67.085 ft
(travel-distance-simple 1 45 0)
(meters-to-feet (travel-distance-simple 1 45 0))

; 0 m, 0 ft
(travel-distance-simple 1 45 90)
(meters-to-feet (travel-distance-simple 1 45 90))

; 207.63 m, 685.17 ft
(travel-distance-simple 1 45 45)
(meters-to-feet (travel-distance-simple 1 45 45))


;; Part 5: Write a procedure that takes an initial elevation and an initial
;; velocity and finds the best angle at which to hit the baseball to optimize
;; the distance traveled.
;; these sound pretty impressive, but we need to look at it more carefully

(define alpha-increment 0.01)
(define max (lambda (x y) (if (> x y) x y)))
(define fba-helper
  (lambda (velocity elevation rad best)
    (define best-distance (travel-distance-simple elevation velocity (radian2degree best)))
    (define rad-distance  (travel-distance-simple elevation velocity (radian2degree rad)))
    (if (> rad (/ pi 2))
	best
	(fba-helper velocity elevation (+ rad alpha-increment) (if (> rad-distance best-distance) rad best)))))

(define find-best-angle
  (lambda (velocity elevation)
    (radian2degree (fba-helper velocity elevation 0 0))))

(find-best-angle 45 0)
(find-best-angle 100 15)
(find-best-angle 100000 5)

; Conclusion: 45 degrees is the best angle to hit the baseball to
; optimize distance traveled.


;; Part 6: Write procedures to calculate travel distance using integration such that
;; you account for drag on the baseball

;; Newton's equations basically say that ma = F, and here F is really two 
;; forces.  One is the effect of gravity, which is captured by mg.  The
;; second is due to drag, so we really have
;;
;; a = drag/m + gravity
;;
;; drag is captured by 1/2 C rho A vel^2, where
;; C is the drag coefficient (which is about 0.5 for baseball sized spheres)
;; rho is the density of air (which is about 1.25 kg/m^3 at sea level 
;; with moderate humidity, but is about 1.06 in Denver)
;; A is the surface area of the cross section of object, which is pi D^2/4 
;; where D is the diameter of the ball (which is about 0.074m for a baseball)
;; thus drag varies by the square of the velocity, with a scaling factor 
;; that can be computed

;; We would like to again compute distance , but taking into account 
;; drag.
;; Basically we can rework the equations to get four coupled linear 
;; differential equations
;; let u be the x component of velocity, and v be the y component of velocity
;; let x and y denote the two components of position (we are ignoring the 
;; third dimension and are assuming no spin so that a ball travels in a plane)
;; the equations are
;;
;; dx/dt = u
;; dy/dt = v
;; du/dt = -(drag_x/m + g_x)
;; dv/dt = -(drag_y/m + g_y)
;; we have g_x = - and g_y = - gravity
;; to get the components of the drag force, we need some trig.
;; let speeed = (u^2+v^2)^(1/2), then
;; drag_x = - drag * u /speed
;; drag_y = - drag * v /speed
;; where drag = beta speed^2
;; and beta = 1/2 C rho pi D^2/4
;; note that we are taking direction into account here

;; we need the mass of a baseball -- which is about .15 kg.

;; so now we just need to write a procedure that performs a simple integration
;; of these equations -- there are more sophisticated methods but a simple one 
;; is just to step along by some step size in t and add up the values

;; dx = u dt
;; dy = v dt
;; du = - 1/m speed beta u dt
;; dv = - (1/m speed beta v + g) dt

;; initial conditions
;; u_0 = V cos alpha
;; v_0 = V sin alpha
;; y_0 = h
;; x_0 = 0

;; we want to start with these initial conditions, then take a step of size dt
;; (which could be say 0.1) and compute new values for each of these parameters
;; when y reaches the desired point (<= 0) we stop, and return the distance (x)

(define drag-coeff 0.5)
(define density 1.06)  ; kg/m^3
(define mass .15)  ; kg
(define diameter 0.074)  ; m
(define beta (* .5 drag-coeff density (* 3.14159 .25 (square diameter))))

(define dpos (lambda (vel dt) (* vel dt)))
(define dvel 
  (lambda (u v dt vel m beta g)
    (* dt
       (- (* (/ (- 1) m)
	     (sqrt (+ (square u) (square v)))
	     vel
	     beta)
	  g))))

(define integrate-helper
  (lambda (x y u v dt g m beta)
    (if (< y 0)
	x
	(integrate-helper (+ x (dpos u dt))
			  (+ y (dpos v dt))
			  (+ u (dvel u v dt u m beta 0))
			  (+ v (dvel u v dt v m beta g))
			  dt
			  g
			  m
			  beta))))
			 
(define integrate
  (lambda (x0 y0 u0 v0 dt g m beta)
    (integrate-helper x0 y0 u0 v0 dt g m beta)))

(define travel-distance
  (lambda (elevation velocity angle)
    (define rad (degree2radian angle))
    (integrate 0
	       elevation
               (* velocity (cos rad))
	       (* velocity (sin rad))
	       0.01
	       gravity
	       mass
	       beta)))

;; Test Cases
(travel-distance 0 45 45)
;Value: 93.23399030748044

(travel-distance 0 40 45)
;Value: 82.31198096995477

(travel-distance 0 35 45)
;Value: 70.59227200484624

; For what range of angles will the ball land over the fence for a batter
; swinging at 45m/s if the fence is 300 ft from home plate?
(define ootp-helper
  (lambda (e v a)
    (display "Angle: ") (display a) (display " ") (display (meters-to-feet (travel-distance e v a)))
    (newline)
    (if (> a 90)
	 90
	 (ootp-helper e v (+ a 1)))))

(define out-of-the-park
  (lambda ()
    (ootp-helper 0 45 0)))

(out-of-the-park)

; Angles between 30-48 degrees, inclusive, will hit the ball out of the park

;; what about Denver?
; In Denver, you can hit it out of the park with angles between 25-55 degrees,
; inclusive.

;; Part 7: Write a set of procedures using the previous integration idea to
;; determine the time it takes to throw the ball a certain distance (within some
;; margin of error). Given an input velocity and desired distance (plus other
;; parameters like the mass of the ball, the beta coefficient, gravity, and the
;; height at which the throw was made) we want to try different initial angles 
;; at which to throw. If the throwing angle will result in the ball traveling
;; roughly the desired distance then we want to find the time it takes for the
;; ball to reach the target using this trajectory. Finally, we want to find the
;; trajectory that results in the shortest time, given a fixed initial velocity.
;; If no angle satisfies the inputs, return 0.
(define travel-distance-generic
  ;; Elevation, Velocity, Angle, Start-X, Time step, Gravity, Mass, Beta
  (lambda (e v a x t g m b)
    (define rad (degree2radian a))
    (integrate x
	       e
               (* v (cos rad))
	       (* v (sin rad))
	       t
	       g
	       m
	       b)))

(define err 2)
(define afd-helper
  ;; Velocity, Distance, Mass, Beta Coefficient, Gravity, Elevation, Angle, List of Angles
  (lambda (v d m b g e a l)
    (if (> a 90)
	l
	(let ((td (travel-distance-generic e v a 0 0.01 gravity mass beta)))
	  (let ((diff (- td d)))
	    (if (and (> diff 0) (< diff err))
		(afd-helper v d m b g e (+ a 1) (cons a l))
		(afd-helper v d m b g e (+ a 1) l)))))))


(define angles-for-distance
  ;; Velocity, Distance, Mass, Beta Coefficient, Gravity, elevation
  (lambda (v d m b g e)
    (afd-helper v d m b g e 0 '())))

(angles-for-distance 45 36 mass beta gravity 0)
(define find-best-angle
  (lambda (loa v e ba)
    (if (null? loa)
	(if (pair? ba)
	    ba
	    (error "No best angle found"))
	(let ((time (time-to-impact (* v (sin (degree2radian (car loa)))) e)))
	  (if (not (pair? ba))
	      (find-best-angle (cdr loa) v e (cons (car loa) time))
	      (if (< time (cdr ba))
		  (find-best-angle (cdr loa) v e (cons (car loa) time))
		  (find-best-angle (cdr loa) v e ba)))))))

(define time-to-throw
  (lambda (v d m b g e)
    (let ((best (find-best-angle (angles-for-distance v d m b g e) v e '())))
      (display "Best angle: ")
      (display (car best))
      (newline)
      (display "Time: ")
      (display (cdr best))
      (newline)
      (cdr best))))



;; If your catcher has a gun for an arm, and can throw at 100 mph (45 m/s), how
;; long does it take to reach second base (36m away)? What if he throws at 35 m/s
;; or 55 m/s?
(time-to-throw 45 36 mass beta gravity 0)
(time-to-throw 35 36 mass beta gravity 0)
(time-to-throw 55 36 mass beta gravity 0)

;; A really good base runner can get from first to second base in 3 seconds. If
;; the catcher throws at 90 mph (40.23 m/s), how much time does he have to catch
;; and release the ball if he is going to put out a runner trying to steal 
;; second?
(- 3 (time-to-throw 40.23 36 mass beta gravity 0))

; The catcher has approximately 1.9 seconds to catch and release the ball to put
; out a runner attempting to steal second. For this specific problem I had to
; increase my error tolerance because an angle couldn't be found to get the ball
; within 1 meter of the target

;; Suppose an outfielder has a strong arm and can throw at 45 m/s. How quickly
;; can he throw the ball to a target at a distance of 30m? 60m? 80m? What if he
;; can throw at 55 m/s? How about a weaker outfielder that can throw at 35 m/s?
(time-to-throw 45 30 mass beta gravity 0)
(time-to-throw 45 60 mass beta gravity 0)
(time-to-throw 45 80 mass beta gravity 0)
; No good angle for this one
; (time-to-throw 55 30 mass beta gravity 0)
(time-to-throw 55 60 mass beta gravity 0)
(time-to-throw 55 80 mass beta gravity 0)
(time-to-throw 35 30 mass beta gravity 0)
(time-to-throw 35 60 mass beta gravity 0)
; No good angle for this one
; (time-to-throw 35 80 mass beta gravity 0)

;; Part 8: Write a procedure that takes an initial velocity, angle of throw,
;; initial height, and number of bounces and returns the distance traveled by
;; the ball. Use this to figure out how far a fielder can throw given one
;; bounce, two bounces, or an arbitrary number of bounces. Try different initial
;; velocities and different angles.
(define bd-helper
  ;; Velocity, Angle, Elevation, Bounces, Distance Traveled
  (lambda (v a e b d)
    (if (or (<= b 0) (<= v 1))
	d
	(bd-helper (/ v 2) a e (- b 1) (+ d (travel-distance-generic e v a 0 0.01 gravity mass beta))))))

(define bounce-distance
  ;; Velocity, Angle, Elevation, Bounces
  (lambda (v a e b)
    (bd-helper v a e b 0)))

(bounce-distance 35 22 0 1)
(bounce-distance 35 22 0 2)
(bounce-distance 35 22 0 10)
(bounce-distance 45 22 0 1)
(bounce-distance 45 22 0 2)
(bounce-distance 45 22 0 10)
(bounce-distance 45 45 0 1)
(bounce-distance 45 45 0 2)
(bounce-distance 45 45 0 10)

;; Part 9: We previously assumed velocity would decrease by half each bounce,
;; but since we're integrating trajectories already we can compute the actual
;; velocity of the ball after a bounce.  Improve the code from part 8 to use
;; this computed velocity and re-run your test cases.

; This is wasteful to do the entire simulation over again just for v but 
; it works and was easy to copy-paste
(define change-in-v
  (lambda (x y u v dt g m beta)
    (if (< y 0)
	(sqrt (+ (square u) (square v)))
	(integrate-helper (+ x (dpos u dt))
			  (+ y (dpos v dt))
			  (+ u (dvel u v dt u m beta 0))
			  (+ v (dvel u v dt v m beta g))
			  dt
			  g
			  m
			  beta))))

(define bd-helper2
  ;; Velocity, Angle, Elevation, Bounces, Distance Traveled
  (lambda (v a e b d)
    (if (or (<= b 0) (<= v 1))
	d
	(bd-helper2 (+ v (change-in-v 0 0 (* v (cos a)) (* v (sin a)) 0.01 gravity mass beta)) a e (- b 1) (+ d (travel-distance-generic e v a 0 0.01 gravity mass beta))))))

(define bounce-distance2
  ;; Velocity, Angle, Elevation, Bounces
  (lambda (v a e b)
    (bd-helper2 v a e b 0)))

(bounce-distance2 35 22 0 1)
(bounce-distance2 35 22 0 2)
(bounce-distance2 35 22 0 10)
(bounce-distance2 45 22 0 1)
(bounce-distance2 45 22 0 2)
(bounce-distance2 45 22 0 10)
(bounce-distance2 45 45 0 1)
(bounce-distance2 45 45 0 2)
(bounce-distance2 45 45 0 10)
