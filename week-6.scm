(define range-tag 'range)
(define make-range (lambda (lower upper)
  (list range-tag lower upper)))

(define make-canonical-limited-precision
  (lambda (val err)
    (make-range (- val err) (+ val err))))

(define make-canonical-constant
  (lambda (c)
    (make-range c c)))
(define range-exp? (lambda (exp)
  (and (pair? exp)
       (eq? (car exp) range-tag))))        

(define *output-ranges-as-limited-precision* #t)
(define output-value
  (lambda (exp)
    (if (range-exp? exp)
	(cond ((equal? (range-min exp) (range-max exp)) (list constant-tag (range-min exp)))
	      ((*output-ranges-as-limited-precision*) exp)
	      ((not *output-ranges-as-limited-precision*)
	       (list limited-tag
		     (+ (range-min exp) (/ (- (range-max exp) (range-min exp)) 2))
		     (/ (- (range-max-exp) (range-min exp)) 2)))
	      (else error))
	(error))))

(output-value 4)

(define (+rat1 r1 r2)
  (define (simplify r)
    (make-rat (/ (num r) (gcd (num r) (denom r))) (/ (denom r) (gcd (num r) (denom r)))))
  (if (and (rat? r1) (rat? r2))
      (simplify (make-rat (+ (* (num r1) (denom r2)) (* (num r2) (denom r1)))
                (* (denom r1) (denom r2))))
      (error "Both inputs need to be rational")))

(define (add n1 n2)
  (cond ((and (integer? n1) (integer? n2)) (make-integer (+ (number n1) (number n2))))
	((and (rat? n1) (rat? n2)) (+rat1 n1 n2))
	((and (integer? n1) (rat? n2)) (+rat1 (make-rat (number n1) 1) n2))
	((and (rat? n1) (integer? n2)) (+rat1 n1 (make-rat (number n2) 1)))
	(else (error "Invalid inputs"))))

(define (find-assoc key alist)
  (cond
   ((null? alist) #f)
   ((equal? key (caar alist)) (cadar alist))
   (else (find-assoc key (cdr alist)))))

(find-assoc 'foo '((bar 6) (fred 10)))
(find-assoc 'foo '((bar 6) (foo #f) (fred #t)))

(define find-assoc-2
  (lambda (key alist)
    (cond ((null? alist) #f)
	  ((equal? key (caar alist)) (car alist))
	  (else (find-assoc-2 key (cdr alist))))))

(find-assoc-2 'foo '((bar 6) (fred 10)))
(find-assoc-2 'foo '((bar 6) (foo #f) (fred #t)))
(define my-list '((bar 6) (foo #f) (fred #t)))

(null? my-list)
(caar my-list)
(car my-list)
      
(define lookup
  (lambda (key alist)
    (cond ((null? alist) (list #f #f))
	  ((equal? key (caar alist)) (list #t (cadar alist)))
	  (else (lookup key (cdr alist))))))

(define lookup
  (lambda (key alist)
    (let ((res (find-assoc-2 key alist)))
      (if (not res)
	  (list #f #f)
	  (list #t (cadr res))))))

(define table3-get 
  (lambda (table key1)
    (if (and (table3? table) (integer? key1))
	(if (and (>= key1 0) (<= key1 (vector-length (caddr table))))
	    (vector-ref (caddr table) key1)
	    'error)
	'error)))

(define table3-put!
  (lambda (table key value)
    (if (and (table3? table) (integer? key) (<= 0 key) (<= key (vector-length (caddr table))))
	(vector-set! (caddr table) key value)
	'error)))

(define make-segment
  (lambda (name start finish)
    (list name start finish)))

(define make-point
  (lambda (x y)
    (list x y)))

(define make-map
  (lambda (segments)
    (list segments)))

(define make-trip
  (lambda (segments)
    (list segments)))

(define segment-name
  (lambda (segment)
    (car segment)))

(define segment-start-point
  (lambda (segment)
    (cadr segment)))

(define segment-end-point
  (lambda (segment)
    (caddr segment)))

(define x-coor
  (lambda (point)
    (car point)))

(define y-coor
  (lambda (point)
    (cadr point)))

(define segment-start-x
  (lambda (seg)
    (x-coor (segment-start-point seg))))

(define segment-end-x
  (lambda (seg)
    (x-coor (segment-end-point seg))))

(define remove-elt
  (lambda (elt lst)
    (define helper
      (lambda (head tail elt)
	(if (null? tail)
	    head
	    (if (equal? elt (car tail))
		(append head (list-tail tail 1))
		(helper (append head (list (car tail))) (cdr tail) elt)))))
    (helper '() lst elt)))

(define map-and-flatten
  (lambda (fun lst)
    (fold-right append '() (map fun lst))))

(define make-match list)
(define first-of-match car)
(define second-of-match cadr)


; This is close, but doesn't work in one case
(define all-correspondences
  (lambda (l1 l2)
    (if (null? l1)
        (list '())
        (map-and-flatten
         (lambda (choice-from-l2)
           (let ((initial-match (make-match (car l1) choice-from-l2))
                 (correspondences-of-remaining-elts
                  (all-correspondences (cdr l1)
                                       (remove-elt choice-from-l2 l2))))
             (list (map append (list initial-match) correspondences-of-remaining-elts))))
          l2))))



(define all-correspondences
  (lambda (l1 l2)
    (if (null? l1)
        (list '())
	(map-and-flatten
	 (lambda (choice-from-l2)
	   (let ((initial-match (make-match (car l1) choice-from-l2))
		 (correspondences-of-remaining-elts
		  (all-correspondences (cdr l1) 
				       (remove-elt choice-from-l2 l2))))
	     (map append (all-correspondences (list initial-match) correspondences-of-remaining-elts))))
          l2))))

(all-correspondences '(a) '(1 2))

    
