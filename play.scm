(cons () ())
(cons 1 (cons 2 (cons 3 ())))
(cons 1 (cons 2 (cons (cons 3 ()) ())))
(cons 1 (cons (cons 2 ()) ()))
(cons 1 (cons (cons 2 (cons 3 ())) ()))
(cons 1 (cons (cons 2 
		    (cons 3 ())) 
	      (cons (cons (cons 4 ()) ()) 
		    ())))
(cons () (cons 1 (cons 2 (cons 3 ()))))

(define contains?
  (lambda (lst elt) 
    (cond ((= (length lst) 0) #f)
          ((equal? (list-ref lst 0) elt) #t)
          (else (contains? (list-tail lst 1) elt)))))

(define remove-duplicates
  (lambda (lst)
    (cond ((<= (length lst) 1) lst)
	  ((contains? (list-tail lst 1) (first lst))
	     (remove-duplicates (list-tail lst 1)))
	  (else (append (list (first lst)) (remove-duplicates (list-tail lst 1)))))))

(remove-duplicates (list 1 2 3 1 2 3))

(define subset?
  (lambda (lst1 lst2)
    (cond ((= (length lst1) 0) #t)
          ((contains? lst2 (first lst1)) (subset? (list-tail lst1 1) lst2))
	  (else #f))))
(subset? (list 1 2 3) (list 5 3 2 1))
(subset? (list 1 2 3) (list 5 4 1))

(define intersection
  (lambda (lst1 lst2)
    (cond ((< (length lst1) 1) lst1)
	  ((contains? lst2 (first lst1)) (append (list (first lst1)) (intersection (list-tail lst1 1) lst2)))
	  (else (intersection (list-tail lst1 1) lst2)))))

(intersection (list 1 2 3) (list 5 3 2 1))
(intersection (list 1 2 3) (list 5 4 1))
(intersection (list 3 2 1) (list 9 10))

(define every-other
  (lambda (lst)
    (cond ((= (length lst) 0) lst)
	  ((< (length lst) 3) (list (first lst)))
	  (else (append (list (first lst)) (every-other (list-tail lst 2)))))))

(every-other (list 1 2 3 4 5))
(every-other (list 2 1 3 6)) 

(define lengths
  (lambda (lst) (map string-length lst)))

(lengths (list "I" "really" "like" "6.001"))

(define total-length
  (lambda (lst) (fold-right + 0 (lengths lst))))

(total-length (list "I" "really" "like" "6.001"))

(define count-true
  (lambda (pred lower upper)
    (cond ((= lower upper) 0)
	  (else (+ (if (pred lower) 1 0)
		   (count-true pred (+ lower 1) upper))))))
(count-true (lambda (x) (even? x)) -37 59)

(define ct-helper
  (lambda (p l u c)
    (cond ((> l u) c)
	  (else (ct-helper p (+ l 1) u (if (p l) (+ c 1) c))))))
(define ct (lambda (p l u) (ct-helper p l u 0)))
(ct (lambda (x) (even? x)) -37 50)

(define accumulate-interval 
  (lambda (op init lower upper)
    (cond ((> lower upper) init)
	  (else (accumulate-interval op (op init lower) (+ lower 1) upper)))))
    
(accumulate-interval + 0 2 4)
(accumulate-interval * 1 2 5)

(define inc (lambda (x) (+ x 1)))
(define compose (lambda (f g) (lambda (x) (f (g x)))))
(define repeatedly-apply 
  (lambda (p n) 
    (lambda (x)
      (cond ((= n 0) x)
	    (else ((compose (repeatedly-apply p (- n 1)) p) x))))))
((repeatedly-apply inc 10) 2)


(define curry
  (lambda (p)
    (lambda (a)
      (lambda (b)
	(p a b)))))

(define plus (lambda (a b) (+ a b)))
(((curry plus) 2) 3)

(define tree (list (list (list 1 2) 3) (list 4 (list 5 6)) 7 (list 8 9 10)))
(display tree)
(define first car)
(define second (lambda (x) (car (cdr x))))
(define third (lambda (x) (car (cdr (cdr x)))))
(define fourth (lambda (x) (car (cdr (cdr (cdr x))))))

(second (first tree))
(first (second (second tree)))
(third tree)
(third (fourth tree))

(define mil-helper
  (lambda (n start lst)
    (if (= 0 n)
	lst
	(mil-helper (- n 1) (+ start 1) (append lst (list start))))))
(define make-increasing-list
  (lambda (n start)
    (mil-helper n start ())))

(make-increasing-list 5 4)

(define mim-helper
  (lambda (rows cols start mat)
    (if (= 0 rows)
	mat
	(mim-helper rows (- cols 1) (+ start rows) (append mat (list (make-increasing-list cols start)))))))

(define make-increasing-matrix
  (lambda (rows cols start)
    (mim-helper rows cols start ())))

(make-increasing-matrix 3 5 10)

(define *english-to-french*
  '((cat chat) (cake gateau) (present cadeau) (I je) (eat mange) (the le)))

(define lookup
  (lambda (word dictionary)
    (cond ((null? dictionary) #f)
	  ((equal? word (car (car dictionary))) (car (cdr (car dictionary))))
	  (else (lookup word (cdr dictionary))))))
(lookup 'cake *english-to-french*)
(lookup 'transmogrify *english-to-french*)

(define translate
  (lambda (sentence dictionary)
    (map lookup sentence dictionary)))

(translate '(I eat the cat) *english-to-french*)

(define lookup
  (lambda (word dictionary)
    (cond ((null? dictionary) #f)
          ((equal? word (car (car dictionary))) (car (cdr (car dictionary))))
          (else (lookup word (cdr dictionary))))))

(define make-histogram
  (lambda (document)
    (add-words-to-histogram document (make-empty-histogram))))

(define make-empty-histogram
  (lambda () '()))

(define in-histogram?
  (lambda (word histogram)
    (and (pair? histogram)
	 (or (eq? word (first (car histogram)))
	     (in-histogram? word (cdr histogram))))))

(define add-new-word-to-histogram
  (lambda (word histogram)
    (cons (list word 1) histogram)))

(define increment-word-count-in-histogram
  (lambda (word histogram)
    (define (helper word remaining processed)
      (cond ((null? remaining) processed)
	    ((equal? word (car (car remaining))) (append (list (list word (+ 1 (car (cdr (car remaining))))) processed (cdr remaining))))
	    (else (helper word (cdr remaining) (append processed (car remaining))))))
    (helper word histogram '())))

(increment-word-count-in-histogram 'dog '((dog 3) (pig 2) (cat 9)))

;(define increment-word-count-in-histogram
;  (lambda (word histogram)
;    (if (in-histogram? word histogram)
;	 (increment-word-count-in-histogram word histogram)
;	 (add-new-word-to-histogram word histogram))))

(define z '(a b))
z
(define y (cons 1 (cons 2 ())))
y
(cons y z)

(define c 'a)
(define d 'b)
(define e (cons d ()))
(define f (cons c d))
(define g (cons e ()))
(define h (cons f g))
h
'((a b) (b))
(list (list 'a 'b) (list 'b))
(let ((z '(a b))) (list z (list 'b)))
(let ((z '(a b))) (list z (cdr z)))
(let ((z (list (list 'a 'b)))) (set-cdr! z (cdar z)) z)
(let ((z (list (list 'a 'b)))) (set-cdr! z (list (cdar z))) z)

(define last-cons
  (lambda (l)
    (if (pair? l)
	(if (null? (cdr l))
	    l
	    (last-cons (cdr l)))
	(error "Arg is not a list"))))
(last-cons '(a b c))
(last-cons '(a))

(define fill-queue!
  (lambda (queue elts)
    (if (null? elts)
	queue
	(fill-queue! (insert-queue! queue (car elts)) (cdr elts)))))

(define (my-cons a b)
  (lambda (msg)
    (cond ((eq? msg 'car) a)
	  ((eq? msg 'cdr) b)
	  ((eq? msg 'set-car!)
	   (lambda (new) (set! a new)))
	  ((eq? msg 'set-cdr!)
	   (lambda (new) (set! b new))))))
(define test (my-cons 1 2))

(define (my-car p)
  (p 'car))

(my-car test)

(define (my-cdr p)
  (p 'cdr))
(my-cdr test)

(define (my-set-car! p new)
  ((p 'set-car!) new))
(my-set-car! test 3)
(my-car test)
(define a 10)
a
(let ((a 6) (b (+ a 2))) (set! a b) a)

(define make-clock
  (lambda ()
    (define msg 'tock)
    (lambda ()
      (if (eq? msg 'tick)
	  (set! msg 'tock)
	  (set! msg 'tick))
      msg)))
(define timex (make-clock))
(timex)

(define make-delay
  (lambda (init)
    (define val init)
    (lambda (new)
      (let ((msg val))
	(set! val new)
	msg))))

(define later (make-delay 'fizz))
(later 6)
(later 'hi)
(later 93)


















