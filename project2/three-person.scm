;;; Student: Walt Elam
;;; TA:
;;; Section:
;;; Project: 2

;; The play-loop procedure takes as its arguments three prisoner's dilemma
;; strategies and plays an itereated game of approximately one hundred rounds.
;; A strategy is a procedure that takes two arguments: a history of the player's
;; previous plays and a history of the other player's previous plays. The
;; procedure returns either a "c" for cooperate, or a "d" for defect
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define *game-association-list*
  (list (list (list "c" "c" "c") (list 4 4 4))
        (list (list "c" "c" "d") (list 2 2 5))
        (list (list "c" "d" "c") (list 2 5 2))
        (list (list "d" "c" "c") (list 5 2 2))
        (list (list "c" "d" "d") (list 0 3 3))
        (list (list "d" "c" "d") (list 3 0 3))
        (list (list "d" "d" "c") (list 3 3 0))
        (list (list "d" "d" "d") (list 1 1 1))))

(define (get-player-points num game)
  (list-ref (get-point-list game) num))

(define (get-point-list game)
  (cadr (extract-entry game *game-association-list*)))

(define make-play list)

(define the-empty-history '())

(define extend-history cons)

(define empty-history? null?)

(define most-recent-play car)

(define rest-of-plays cdr)

(define play-games-helper
  (lambda (strats1 strats2)
    (if (null? strats1)
	()
	(if (null? strats2)
	    (play-games-helper (cdr strats1) (cdr strats1))
	    (begin
	      (display (car strats1))
	      (display " vs. ")
	      (display (car strats2))
	      (newline)
	      (play-loop (car strats1) (car strats2))
	      (play-games-helper strats1 (cdr strats2)))))))

;; this isn't correct for three strats yet
(define play-games
  (lambda (strat-list)
    (define helper
      (lambda (l1 l2 l3)
	(if (null? l1)
	    ()
	    (if (null? l2)
		(helper (cdr l1) (cdr l1) (cdr l1))
		(if (null? l3)
		    (helper l1 (cdr l2) (cdr l2))
		    (begin
		      (display (car l1))
		      (display " vs. ")
		      (display (car l2))
		      (display " vs. ")
		      (display (car l3))
		      (newline)
		      (play-loop (car l1) (car l2) (car l3))
		      (helper l1 l2 (cdr l3))))))))
    (helper strat-list strat-list strat-list)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  The following procedures are used to compute and print
;;  out the players' scores at the end of an iterated game
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Part 9: Revise the scheme code for the two-player games to make a
;;; three-player iterated game. You need only change three procedures:
;;; play-loop, print-out-results, and get-scores. You may have to change your
;;; definition of extract-entry if you did not write it in a general enough
;;; manner.

(define (play-loop strat0 strat1 strat2)
  (define (play-loop-iter strat0 strat1 strat2 count history0 history1 history2 limit)
    (cond ((= count limit) (print-out-results history0 history1 history2 limit))
	  (else (let ((result0 (strat0 history0 history1 history2))
		      (result1 (strat1 history1 history0 history2))
		      (result2 (strat2 history2 history0 history1)))
		  (play-loop-iter strat0 strat1 strat2 (+ count 1)
				  (extend-history result0 history0)
				  (extend-history result1 history1)
				  (extend-history result2 history2)
				  limit)))))
  (play-loop-iter strat0 strat1 strat2 0 the-empty-history the-empty-history the-empty-history
		  (+ 90 (random 21))))

(define (print-out-results history0 history1 history2 number-of-games)
  (let ((scores (get-scores history0 history1 history2)))
    (newline)
    (display "Player 1 Score:  ")
    (display (* 1.0 (/ (car scores) number-of-games)))
    (newline)
    (display "Player 2 Score:  ")
    (display (* 1.0 (/ (cadr scores) number-of-games)))
    (newline)
    (display "Player 3 Score:  ")
    (display (* 1.0 (/ (caddr scores) number-of-games)))
    (newline)))

(define (get-scores history0 history1 history2)
  (define (get-scores-helper history0 history1 history2 score0 score1 score2)
    (cond ((empty-history? history0)
	   (list score0 score1 score2))
	  (else (let ((game (make-play (most-recent-play history0)
				       (most-recent-play history1)
				       (most-recent-play history2))))
		  (get-scores-helper (rest-of-plays history0)
				     (rest-of-plays history1)
				     (rest-of-plays history2)
				     (+ (get-player-points 0 game) score0)
				     (+ (get-player-points 1 game) score1)
				     (+ (get-player-points 2 game) score2))))))
  (get-scores-helper history0 history1 history2 0 0 0))

(define extract-entry
  (lambda (play *gal*)
    (cond ((or (null? play) (null? *gal*)) ())
	  ((equal? play (caar *gal*)) (car *gal*))
	  (else (extract-entry play (cdr *gal*))))))

;; Test cases
(define 0-play ())
(define 1-play (make-play "c" "c" "c"))
(define 2-play (make-play "c" "d" "c"))
(define 3-play (make-play "d" "c" "c"))
(define 4-play (make-play "d" "d" "c"))
(define 5-play (make-play "d" "e" "c"))
(extract-entry 0-play *game-association-list*)
(extract-entry 1-play *game-association-list*)
(extract-entry 2-play *game-association-list*)
(extract-entry 3-play *game-association-list*)
(extract-entry 4-play *game-association-list*)
(extract-entry 5-play *game-association-list*)

;;; Part 10: Write strategies Patsy-3, Nasty-3, and Spastic-3 that will work in
;;; a three-player game and test them out. Write two new strategies,
;;; tough-Eye-for-Eye and soft-Eye-for-Eye where the first defects if either
;;; opponent defected on the previous round and the latter defects only if both
;;; opponents defected on the previous round. Play some games with these new
;;; strategies

(define (Nasty-3 me op1 op2)
  "d")

(define (Patsy-3 me op1 op2)
  "c")

(define (Spastic-3 me op1 op2)
  (if (= (random 2) 0)
      "c"
      "d"))

(define (tough-Eye-for-Eye me op1 op2)
  (if (empty-history? me)
      "d"
      (if (or (equal? "d" (most-recent-play op1)) (equal? "d" (most-recent-play op2)))
	  "d"
	  "c")))

(define (soft-Eye-for-Eye me op1 op2)
  (if (empty-history? me)
      "d"
      (if (and (equal? "d" (most-recent-play op1)) (equal? "d" (most-recent-play op2)))
	  "d"
	  "c")))

(define strats (list Nasty-3 Patsy-3 Spastic-3 tough-Eye-for-Eye soft-Eye-for-Eye))
(play-games strats)

;; From skimming through the results it looks like Nasty-3 is the winning
;; strategy even in a three player scenario.

;;; Part 11: Write a procedure make-combined-strategies which takes as input two
;;; two-player strategies and a "combining" procedure, it should return a three-
;;; player strategy that plays one of the two-player strategies against one of
;;; the opponents and the other two player strategy against the other opponent,
;;; finally calling the combining procedure on those two results.

(define make-combined-strategies
  (lambda (s1 s2 combine)
    (lambda (h1 h2 h3)
      (let ((res1 (s1 h1 h2))
	    (res2 (s2 h1 h3)))
	(combine res1 res2)))))

;;; Part 12: A natural idea is to deduce what strategy your opponents are using.
;;; Implement a data structure called a history-summary, which has three
;;; sub-pieces one for each case: both opponents cooperated, both defect, and
;;; one defected while one cooperated. You should have three selectors, one for
;;; each subpiece. Each sub-piece has another structure that keeps track of the
;;; number of times player-0 cooperated on the next round, the number of times
;;; player-0 defected on the next round, and the number of examples (though this
;;; is redundant). You could use a tree structure for this purpose.

(define make-action-item
  (lambda (c-count d-count t-count)
    (list c-count d-count t-count)))
(define new-action-item (make-action-item 0 0 0))
(define make-history-summary
  (lambda (cc-action cd-action dd-action)
    (list cc-action cd-action dd-action)))
(define new-history-summary (make-history-summary new-action-item new-action-item new-action-item))
(define get-c car)
(define get-d cadr)
(define get-t caddr)
(define get-cc car)
(define get-cd cadr)
(define get-dd caddr)
(define inc-c
  (lambda (action)
    (make-action-item (+ 1 (get-c action)) (get-d action) (+ 1 (get-t action)))))
(define inc-d
  (lambda (action)
    (make-action-item (get-c action) (+ 1 (get-d action)) (+ 1 (get-t action)))))


;; in expected-values: #f = don't care 
;;                      X = actual-value needs to be #f or X 
;(define (test-entry expected-values actual-values) 
;   (cond ((null? expected-values) (null? actual-values)) 
;         ((null? actual-values) #f) 
;         ((or (not (car expected-values)) 
;              (not (car actual-values)) 
;              (= (car expected-values) (car actual-values))) 
;          (test-entry (cdr expected-values) (cdr actual-values))) 
;         (else #f))) 
;
;(define (is-he-a-fool? hist0 hist1 hist2) 
;   (test-entry (list 1 1 1) 
;               (get-probability-of-c 
;                (make-history-summary hist0 hist1 hist2))))
;
;(define (could-he-be-a-fool? hist0 hist1 hist2)
;  (test-entry (list 1 1 1)
;              (map (lambda (elt) 
;                      (cond ((null? elt) 1)
;                            ((= elt 1) 1)  
;                            (else 0)))
;                   (get-probability-of-c (make-history-summary hist0 
;                                                               hist1
;                                                               hist2)))))
