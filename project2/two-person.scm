;;; Student: Walt Elam
;;; TA:
;;; Section:
;;; Project: 2

;; 
;;  The play-loop procedure takes as its  arguments two prisoner's
;;  dilemma strategies, and plays an iterated game of approximately
;;  one hundred rounds.  A strategy is a procedure that takes
;;  two arguments: a history of the player's previous plays and 
;;  a history of the other player's previous plays.  The procedure
;;  returns either a "c" for cooperate or a "d" for defect.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(define (play-loop strat0 strat1)
  (define (play-loop-iter strat0 strat1 count history0 history1 limit)
    (cond ((= count limit) (print-out-results history0 history1 limit))
	  (else (let ((result0 (strat0 history0 history1))
		      (result1 (strat1 history1 history0)))
		  (play-loop-iter strat0 strat1 (+ count 1)
				  (extend-history result0 history0)
				  (extend-history result1 history1)
				  limit)))))
  (play-loop-iter strat0 strat1 0 the-empty-history the-empty-history
		  (+ 90 (random 21))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  The following procedures are used to compute and print
;;  out the players' scores at the end of an iterated game
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (print-out-results history0 history1 number-of-games)
  (let ((scores (get-scores history0 history1)))
    (newline)
    (display "Player 1 Score:  ")
    (display (* 1.0 (/ (car scores) number-of-games)))
    (newline)
    (display "Player 2 Score:  ")
    (display (* 1.0 (/ (cadr scores) number-of-games)))
    (newline)))

(define (get-scores history0 history1)
  (define (get-scores-helper history0 history1 score0 score1)
    (cond ((empty-history? history0)
	   (list score0 score1))
	  (else (let ((game (make-play (most-recent-play history0)
				       (most-recent-play history1))))
		  (get-scores-helper (rest-of-plays history0)
				     (rest-of-plays history1)
				     (+ (get-player-points 0 game) score0)
				     (+ (get-player-points 1 game) score1))))))
  (get-scores-helper history0 history1 0 0))

(define (get-player-points num game)
  (list-ref (get-point-list game) num))

(define *game-association-list*
  ;; format is that first sublist identifies the players' choices 
  ;; with "c" for cooperate and "d" for defect; and that second sublist 
  ;; specifies payout for each player
  '((("c" "c") (3 3))
    (("c" "d") (0 5))
    (("d" "c") (5 0))
    (("d" "d") (1 1))))

(define (get-point-list game)
  (cadr (extract-entry game *game-association-list*)))

(define make-play list)

(define the-empty-history '())

(define extend-history cons)

(define empty-history? null?)

(define most-recent-play car)

(define rest-of-plays cdr)

;; A sampler of strategies

(define (NASTY my-history other-history)
  "d")

(define (PATSY my-history other-history)
  "c")

(define (SPASTIC my-history other-history)
  (if (= (random 2) 0)
      "c"
      "d"))

(define (EGALITARIAN  my-history other-history)
  (define (count-instances-of test hist)
    (cond ((empty-history? hist) 0)
	  ((string=? (most-recent-play hist) test)
	   (+ (count-instances-of test (rest-of-plays hist)) 1))
	  (else (count-instances-of test (rest-of-plays hist)))))
  (let ((ds (count-instances-of "d" other-history))
	(cs (count-instances-of "c" other-history)))
    (if (> ds cs) "d" "c")))

(define (EYE-FOR-EYE my-history other-history)
  (if (empty-history? my-history)
      "c"
      (most-recent-play other-history)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Part 1: Write a procedure to retrieve payoff information from the game
;;; association list, e.g.
;;; (define a-play (make-play "c" "d"))
;;; (extract-entry a-play *game-association-list*)
;;; ;Value: (("c" "d") (0 5))
;;;

(define extract-entry
  (lambda (play *gal*)
    (cond ((or (null? play) (null? *gal*)) ())
	  ((equal? play (caar *gal*)) (car *gal*))
	  (else (extract-entry play (cdr *gal*))))))

;; Test cases
(define 0-play ())
(define 1-play (make-play "c" "c"))
(define 2-play (make-play "c" "d"))
(define 3-play (make-play "d" "c"))
(define 4-play (make-play "d" "d"))
(define 5-play (make-play "d" "e"))
(extract-entry 0-play *game-association-list*)
(extract-entry 1-play *game-association-list*)
(extract-entry 2-play *game-association-list*)
(extract-entry 3-play *game-association-list*)
(extract-entry 4-play *game-association-list*)
(extract-entry 5-play *game-association-list*)

;;; Part 2: Use play-loop to play games among the five defines strategies.
;;; Create a matrix in which you show the average score for tournaments pitting
;;; all possible pairing of the five different strategies. Describe the behavior
;;; for the different strategies.
;;;
(define strats (list NASTY PATSY SPASTIC EGALITARIAN EYE-FOR-EYE))

(define play-games-helper
  (lambda (strats1 strats2)
    (if (null? strats1)
	()
	(if (null? strats2)
	    (play-games-helper (cdr strats1) (cdr strats1))
	    (begin
	      (display (car strats1))
	      (display " vs. ")
	      (display (car strats2))
	      (newline)
	      (play-loop (car strats1) (car strats2))
	      (play-games-helper strats1 (cdr strats2)))))))

(define play-games
  (lambda (strat-list)
    (play-games-helper strat-list strat-list)))

;; Test Cases
(play-games (list))
(play-games strats)

;; Results
;; Row x Col = Row score against Col
;;            |   NASTY   |   PATSY   |  SPASTIC  |EGALITARIAN|EYE-FOR-EYE|  Sum
;;    NASTY   |     1     |     5     |   2.750   |   1.038   |   1.043   | 10.831
;;    PATSY   |     0     |     3     |   1.333   |     4     |     3     | 11.333
;;   SPASTIC  |   .5625   |   4.111   |   2.332   |   3.706   |   2.351   | 13.063
;; EGALITARIAN|   .9904   |     3     |   1.734   |     3     |     3     | 11.724
;; EYE-FOR-EYE|   .9891   |     3     |   2.351   |     3     |     3     | 12.340
;;
;; The highest overall score came from using the SPASTIC strategy which is
;; motivating since it isn't intelligent at all. The other strategies did
;; almost as well as each other with the clear winner being NASTY which
;; is sad.

;;; Part 3: Why are games involving EGALITARIAN slower than other games?
;;; Compare this iterative version of EGALITARIAN with the original one. Is it
;;; faster? Do the orders of growth in time differ?
;;; (define (EGALITARIAN-I my-history other-history)
;;;   (define (majority-loop cs ds hist)
;;;     (cond ((empty-history? hist) (if (> ds cs) "d" "c"))
;;;           ((string=? (most-recent-play hist) "c")
;;;            (majority-loop (+ 1 cs) ds (rest-of-plays hist)))
;;;           (else
;;;            (majority-loop cs (+ 1 ds) (rest-of-plays hist)))))
;;;   (majority-loop 0 0 other-history))
;;;

;; The EGALITARIAN strategy takes so long because it performs two list walks to
;; determine the history; each successive calls takes one more iteration for
;; each of those loops. The iterative version is much faster because it tracks
;; the score history and only does a comparison against the most recent play,
;; rather than re-computing the history score each time.

;;; Part 4: Write a new strategy, EYE-FOR-TWO-EYES. The strategy should always
;;; cooperate unless the opponent defected on both of the previous two rounds.
;;; Play this against the other strategies.
;;;

(define (EYE-FOR-TWO-EYES my-history other-history)
  (cond ((empty-history? my-history) "c")
	((empty-history? (rest-of-plays my-history)) "c")
	((and (string=? "d" (most-recent-play other-history))
	      (string=? "d" (most-recent-play (rest-of-plays other-history)))) 
	 "d")
	(else "c")))

;; Test cases
(define strats (list NASTY PATSY SPASTIC EGALITARIAN EYE-FOR-EYE EYE-FOR-TWO-EYES))
(play-games strats)

;; Results
;; Row x Col = Row score against Col
;;            |   NASTY   |   PATSY   |  SPASTIC  |EGALITARIAN|EYE-FOR-EYE|EYE-42-EYES|  Sum
;;    NASTY   |     1     |     5     |   2.750   |   1.038   |   1.043   |   1.088   | 11.919
;;    PATSY   |     0     |     3     |   1.333   |     4     |     3     |     3     | 14.333
;;   SPASTIC  |   .5625   |   4.111   |   2.332   |   3.706   |   2.351   |   3.133   | 16.196
;; EGALITARIAN|   .9904   |     3     |   1.734   |     3     |     3     |     3     | 14.724
;; EYE-FOR-EYE|   .9891   |     3     |   2.351   |     3     |     3     |     3     | 15.340
;; EYE-42-EYES|   .9780   |     3     |   1.800   |     3     |     3     |     3     | 14.778
;;
(+ 1 5 2.750 1.038 1.043 1.088)
(+ 0 3 1.333 4 3 3)
(+ .5625 4.111 2.332 3.706 2.351 3.133)
(+ .9904 3 1.734 3 3 3)
(+ .9891 3 2.351 3 3 3)
(+ .9780 3 1.800 3 3 3)
;; EYE-FOR-TWO-EYES just slightly better than the regular strategy, but not by
;; much. SPASTIC Is still a clear loser while NASTY remains to be the leader.

;;; Part 5: Write a procedure make-eye-for-n-eyes which takes a number as input
;;; and returns the appropriate EYE-FOR-EYE-like strategy. For example,
;;; (make-eye-for-n-eyes 2) should return a strategy equivalent to
;;; EYE-FOR-TWO-EYES.

(define last-n-d?
  (lambda (history n)
    (cond ((empty-history? history) #f)
	  ((string=? "d" (most-recent-play history))
	   (last-n-d? (rest-of-plays history) (- n 1)))
	  (else #t))))

(define make-eye-for-n-eyes
  (lambda (n)
    (lambda (my-history other-history)
      (if (last-n-d? other-history n)
	  "d"
	  "c"))))

;; Test cases
(define EYE-FOR-TEN-EYES (make-eye-for-n-eyes 10))
(define strats (list NASTY PATSY SPASTIC EGALITARIAN EYE-FOR-EYE EYE-FOR-TWO-EYES EYE-FOR-TEN-EYES))
(play-games strats)

;; This strategy seems to improve against NASTY, scoring a 0 to nastys 5
;; (finally).  I would guess that this strategy will improve to a point, not
;; sure what that point is though.
	
    
;;; Part 6: Write a procedure make-rotating-strategy which takes as input two
;;; strategies and two integers and returns a strategy which plays strat0 for
;;; the first freq0 rounds in the iterated game, then switches to strat1 for the
;;; next freq1 rounds, and so on.

(define make-rotating-strategy
  (lambda (strat0 strat1 freq0 freq1)
    (lambda (my-history other-history)
      (if (<= (remainder (length my-history) freq0) freq0)
	  (strat0 my-history other-history)
	  (strat1 my-history other-history)))))

;;; Part 7: Write a new strategy, make-higher-order-spastic which takes a list
;;; of strategies as input and returns a new strategy that loops through this
;;; list of strategies, using the next one in the list for each play.

(define make-higher-order-spastic
  (lambda (strat-list)
    (lambda (my-history other-history)
      ((list-ref strat-list (remainder (length my-history) (length strat-list))) my-history other-history))))

(define HIGH-SPAZ (make-higher-order-spastic (list NASTY PATSY EGALITARIAN EYE-FOR-EYE)))
(define strats (list NASTY PATSY SPASTIC EGALITARIAN EYE-FOR-EYE HIGH-SPAZ))
(play-games strats)

;; Results
;; Row x Col = Row score against Col
;;            |   NASTY   |   PATSY   |  SPASTIC  |EGALITARIAN|EYE-FOR-EYE| HIGH-SPAZ |  Sum
;;    NASTY   |     1     |     5     |   2.750   |   1.038   |   1.043   |   2.020   | 12.851
;;    PATSY   |     0     |     3     |   1.333   |     4     |     3     |   2.229   | 13.562
;;   SPASTIC  |   .5625   |   4.111   |   2.332   |   3.706   |   2.351   |   2.541   | 15.604
;; EGALITARIAN|   .9904   |     3     |   1.734   |     3     |     3     |   2.263   | 13.987
;; EYE-FOR-EYE|   .9891   |     3     |   2.351   |     3     |     3     |   2.750   | 15.090
;;  HIGH-SPAZ |   .7451   |   3.514   |   2.311   |   3.473   |   2.750   |   2.500   | 15.293
(+ 1 5 2.750 1.038 1.043 2.020)
(+ 0 3 1.333 4 3 2.229)
(+ .5625 4.111 2.332 3.706 2.351 2.541)
(+ .9904 3 1.734 3 3 2.263)
(+ .9891 3 2.351 3 3 2.750)
(+ .7451 3.514 2.311 3.473 2.750 2.5)

;; The more spastic strategy didn't do much better than the normal spastic
;; strategy and didn't show any significance increase or decrease against other
;; strategies.

;;; Part 8: Write a procedure gentle which takes as input a strategy and a
;;; number between 0 and 1 and plays the same as strat except when strat
;;; defects, the new strategy should have a gentleness-factor chance of
;;; cooperating.

(define gentle
  (lambda (strat gentleness-factor)
    (lambda (my-history other-history)
      (let ((norm (strat my-history other-history)))
	    (if (and (equal? 'd norm) (<= (random 1.0) gentleness-factor))
		'c
		norm)))))

(define slightly-gentle-Nasty (gentle NASTY 0.1))
(define slightly-gentle-Eye-for-Eye (gentle EYE-FOR-EYE 0.1))

