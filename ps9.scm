Copyright (C) 2014 Massachusetts Institute of Technology
This is free software; see the source for copying conditions. There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Image saved on Saturday May 17, 2014 at 2:39:25 AM
  Release 9.2 || Microcode 15.3 || Runtime 15.7 || SF 4.41 || LIAR/x86-64 4.118
  Edwin 3.116
;You are in an interaction window of the Edwin editor.
;Type `C-h' for help, or `C-h t' for a tutorial.
;`C-h m' will describe some commands.
;`C-h' means: hold down the Ctrl key and type `h'.
;Package: (user)

(define or-val
  (lambda (e1 e2 e3)
    (if (not (eq? #f (eval e1)))
	e1
	(if (not (eq? #f (eval e2)))
	    e2
	    (if (not (eq? #f (eval e3)))
		e3
		#f)))))
;Value: or-val

(define interleave
  (lambda (s1 s2)
    (cond ((null? s1) s2)
	  ((null? s2) s1)
	  (append (list (car s1) (car s2)) (interleave (cdr s1) (cdr s2))))))